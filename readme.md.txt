<Estelle Nugues> <Cécile Iweins>
Liste des taches effectuées pendant le TP : TP1 
Liste des taches non réalisées : la fonctionnalité getAllBook

Répartition du travail entre les binômes :
- Estelle a travaillé sur une branche A dans la partie 4. Branche qui a été supprimée.
Une nouvelle branche a ensuite été créée (FeatureA2) pour se répartir le travail dans la partie 6.
Dans la partie 6 Estelle a travaillé sur la classe Consumable et les classes héritant de Consumable 
et enfin sur les méthodes de la classe Shop  : isItemAvailable, getNbteminStorage, getQuantityPerConsumable.

- Cécile a travaillé sur une branche B dans la partie 4. Branche qui a été supprimée 
Une nouvelle branche a ensuite été créée (FeatureB2) pour se répartir le travail dans la partie 6.
Dans la partie 6 Cécile a travaillé sur la classe Book et les classes héritant de Book, 
et enfin sur les méthodes de la classe Shop  : getAgeForBook et getAllBook.

- Liste des classes couvertes par des tests unitaires : toutes celles qui ont été développées

Commentaires sur le TP :
- Nous aurions dû mettre tous nos attributs en privé et utiliser les getters et setters pour rendre 
accessible ces attributs dans une autre classe. Nous avons choisi de ne pas modifier le TP en ce sens, 
mais nous avons identifié le problème.