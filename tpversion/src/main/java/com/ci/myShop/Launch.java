package com.ci.myShop;

import com.ci.myShop.model.Consumable;
import com.ci.myShop.model.Item;
import com.ci.myShop.model.book.Book;
import com.ci.myShop.model.book.BookToTouch;
import com.ci.myShop.model.book.MusicalBook;
import com.ci.myShop.model.book.OriginalBook;
import com.ci.myShop.model.book.PuzzleBook;
import com.ci.myShop.controller.Storage;

import java.util.Iterator;
import java.util.List;

import com.ci.myShop.controller.Shop;

public class Launch {

	public static void main(String[] args) {
		
		Storage storage = new Storage();
		float cash = 0;
		Shop shop = new Shop(storage, cash);
		
		Item item1 = new Item("Le Petit Prince",001,10,8);
		/*Item item1 = new Item();
		item1.setName("Le Petit Prince");
		item1.setId(001);
		item1.setPrice(10);
		item1.setNbrElt(8);*/
		
		Item item2 = new Item("Madame Bovary",002,9,15);
		/*Item item2 = new Item();
		item2.setName("Madame Bovary");
		item2.setId(002);
		item2.setPrice(9);
		item2.setNbrElt(15);*/
		
		shop.getStorage().addItem(item1);
		//shop.getStorage().addItem(item2);
		
		shop.sell(item1.getName());
		
		System.out.println("Le cash du shop est : " + shop.getCash());

		if(shop.buy(item2)==true) {
			System.out.println("Le cash restant est :" + shop.getCash());

		}
		
		//v�rifie la disponibilit� du petit prince
		System.out.println("Disponibilit� de "+item1.getName()+" = "+shop.isItemAvailable(item1.getName())); 
		
		//Donne la quantit� d'un �l�ment si il est disponible
		System.out.println("Nombre de "+item1.getName()+" = "+shop.getINbtemInStorage(item1.getName())); 
		 
		//retourne le nombre d'�l�ments par lot pour un consumable
		Consumable consumable1 = new Consumable("Cartouche couleur", 125, 35, 10, 3);
		System.out.println("Nombre de lots dans "+consumable1.getName()+" = "+shop.getQuantityPerConsumable(consumable1.getName())); 
		
		Item item3 = new Item("Germinal",003,9,15);
		shop.getAllItem();

				}

		Book book = new Book();
		book.setName("Tintin");
		book.setAuthor("Hergé");
		book.setId(123);
		book.setAge(15);
		book.setNbPage(100);
		book.setPublisher("Junod");
		book.setYear(2019);
		book.setNbrElt(4);
		book.setPrice(35);
		
		OriginalBook book2 = new OriginalBook();
		book2.setName("Brice");
		book2.setAuthor("Hergé");
		book2.setId(123);
		book2.setAge(16);
		book2.setNbPage(100);
		book2.setPublisher("Junod");
		book2.setYear(2019);
		book2.setNbrElt(4);
		book2.setPrice(35);
		book2.setNumeric(true);
		
	
		shop.getStorage().addItem(book2);
		int age = shop.getAgeForBook("Le Petit Prince");
		System.out.println(age);
		
		
		Item item = storage.getItem("Le Petit Prince");
	
		
		if((item instanceof Book || item instanceof OriginalBook || item instanceof MusicalBook || item instanceof PuzzleBook || item instanceof BookToTouch)==true) {
			Book item3 = (Book) storage.getItem("Brice");
			System.out.println("l'âge mini est :" + item3.getAge() + "ans");
			
			
		}
			else {System.out.println("Cet objet n'est pas un livre");
		}
		
		
		}		
	

	}
	
