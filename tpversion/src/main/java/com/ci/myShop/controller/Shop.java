package com.ci.myShop.controller;

import com.ci.myShop.model.Item;

import com.ci.myShop.model.Consumable;

import com.ci.myShop.model.book.Book;
import com.ci.myShop.model.book.BookToTouch;
import com.ci.myShop.model.book.MusicalBook;
import com.ci.myShop.model.book.OriginalBook;
import com.ci.myShop.model.book.PuzzleBook;

import com.ci.myShop.controller.Storage;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.lang.Float;

public class Shop {

	public Storage getStorage() {
		return storage;
	}
	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	public float getCash() {
		return cash;
	}
	public void setCash(float cash) {
		this.cash = cash;
	}
	
	public Storage storage;
	float cash;
	
	// Constructeur d'un shop avec un stock (storage) et un montant disponible dans la caisse (cash)
	public Shop(Storage storage, float cash) {
		super();
		this.storage = storage;
		this.cash = cash;
	}

	// permet de vendre un item si il existe et si il est en stock
	public Item sell(String name) {
		
		Item u = storage.getItem(name);

		if (u == null || u.getNbrElt()==0) {
			//System.out.println("Plus de stock pour " +name);
			return null;
		}
		else {

			cash = cash + u.getPrice(); 
			//ajoute le prix de vente de l'article au cash
			u.setNbrElt(u.getNbrElt()-1);
			//retire un élément du stock
			return u;
		}
	}

	public boolean buy(Item item)	{
		Storage storage = new Storage();
		if(cash<item.getPrice()) {
			return false;
		}
		else {
			storage.addItem(item);
			item.setNbrElt(item.getNbrElt()+1);
			cash = cash - item.getPrice();
			return true;
		}
	}
	

	// permet de savoir si un Item est disponible
	public boolean isItemAvailable(String name) {
		Item u = storage.getItem(name);
		if (u == null) return false;
		else return true;
	}
	
	//permet de retourner tous les livres du shop
	public void getAllItem(){		
		for (Map.Entry<String,Item> i : storage.itemMap.entrySet()) {
			System.out.println(i.getKey());	
			//Map.Entry<String,Item> i = storage.itemMap.next();
		}	
	}
	
	//permet de retourner la quantit� d'un item disponible dans le storage
	public int getINbtemInStorage(String name) {
		Item u = storage.getItem(name);
		if (isItemAvailable(name)) {
			return u.getNbrElt();
		} else return 0;
	}
	
	//retourne le nombre d'�l�ments par lot
	public int getQuantityPerConsumable(String name) {
		Item item = storage.getItem(name);
		if (item instanceof Consumable==true) {
			Consumable consumable = (Consumable) item;
			return consumable.getQuantity();
		} else return 0;
	}

	public int getAgeForBook(String name) {
		
		Item item = storage.getItem(name);
		if((item instanceof Book || item instanceof OriginalBook || item instanceof MusicalBook || item instanceof PuzzleBook || item instanceof BookToTouch)==true) {
			Book item2 = (Book) storage.getItem(name);
			int age = 0;
			System.out.println("l'âge mini est :" + item2.getAge() + "ans");
			return item2.getAge();
		}
			else {System.out.println("Cet objet n'est pas un livre");
			return (Integer) null;
			}
		}	
		
		/*public List<Book> getAllBook(){
		List<Book> ListeLivre = null;
		Storage storage;
		String name;
		List<Book> list = (List<Book>) storage;
		Iterator it = list.iterator();
		while (it.hasNext()){
		   if(getAgeForBook(storage.getItem.getName())>(-1));
		   }
		return ListeLivre;*/
	
		//manque de connaissances java pour réussir à finir cette méthode.
			

}

	



