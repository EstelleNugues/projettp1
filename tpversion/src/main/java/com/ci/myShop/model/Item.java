package com.ci.myShop.model;

import com.ci.myShop.model.book.Book;

public class Item {

	String name;
	int id;
	float price;
	int nbrElt;
	

	public Item(String name, int id, float price, int nbrElt) {

	//ajout d'un constructeur afin de générer les constructeurs héritants
	public Item(String name, int id, float price, int nbrElt) {
		super();

		this.name = name;
		this.id = id;
		this.price = price;
		this.nbrElt = nbrElt;
	}

	public Item() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getNbrElt() {
		return nbrElt;
	}
	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}


	public String display (String name, int id, float price, int nbrElt) {	
		return (name+"-"+id+"-"+price+"-"+nbrElt);

	}


	
 
}

