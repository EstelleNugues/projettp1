/**
 * 
 */
package com.ci.myShop.model.book;

import com.ci.myShop.model.Item;

/**
 * @author cecilethibaut
 *
 */


public class Book extends Item {
	int NbPage;
	String author;
	String publisher;
	int year;
	int age;
	


	
	public Book(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher, int year,
			int age) {
		super(name, id, price, nbrElt);
		NbPage = nbPage;
		this.author = author;
		this.publisher = publisher;
		this.year = year;
		this.age = age;
	}

	
	public Book(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}


	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	


	public int getNbPage() {
		return NbPage;
	}
	
	public void setNbPage(int nbPage) {
		NbPage = nbPage;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}


}

	


