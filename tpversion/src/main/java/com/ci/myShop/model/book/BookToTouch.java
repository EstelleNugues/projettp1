package com.ci.myShop.model.book;

public class BookToTouch extends Book{
	String materials;
	int durability;
	
	
	public BookToTouch(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age, String materials, int durability) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		this.materials = materials;
		this.durability = durability;
	}
	
	public String getMaterials() {
		return materials;
	}
	public void setMaterials(String materials) {
		this.materials = materials;
	}
	public int getDurability() {
		return durability;
	}
	public void setDurability(int durability) {
		this.durability = durability;
	}
	
	

}
