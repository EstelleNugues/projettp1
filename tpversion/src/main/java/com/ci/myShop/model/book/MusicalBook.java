package com.ci.myShop.model.book;

import java.util.List;

public class MusicalBook extends Book {
	List<String> listofSound;
	int lifetime;
	int nbrBattery;
	
	
	public MusicalBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age, List<String> listofSound, int lifetime, int nbrBattery) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		this.listofSound = listofSound;
		this.lifetime = lifetime;
		this.nbrBattery = nbrBattery;
	}
	
	
	
	
	public MusicalBook() {
		super();
		// TODO Auto-generated constructor stub
	}




	public MusicalBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher,
			int year, int age) {
		super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
		// TODO Auto-generated constructor stub
	}




	public MusicalBook(String name, int id, float price, int nbrElt) {
		super(name, id, price, nbrElt);
		// TODO Auto-generated constructor stub
	}




	public List<String> getListofSound() {
		return listofSound;
	}
	public void setListofSound(List<String> listofSound) {
		this.listofSound = listofSound;
	}
	public int getLifetime() {
		return lifetime;
	}
	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}
	public int getNbrBattery() {
		return nbrBattery;
	}
	public void setNbrBattery(int nbrBattery) {
		this.nbrBattery = nbrBattery;
	}
	
	
	

}
