package com.ci.myShop.model.book;

public class PuzzleBook extends Book {
	
int NbrPieces;


public PuzzleBook(String name, int id, float price, int nbrElt, int nbPage, String author, String publisher, int year,
		int age, int nbrPieces) {
	super(name, id, price, nbrElt, nbPage, author, publisher, year, age);
	NbrPieces = nbrPieces;
}

public int getNbrPieces() {
	return NbrPieces;
}

public void setNbrPieces(int nbrPieces) {
	NbrPieces = nbrPieces;
}


}
