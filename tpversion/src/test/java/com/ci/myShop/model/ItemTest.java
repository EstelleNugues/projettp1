package com.ci.myShop.model;

import org.junit.Test;

public class ItemTest {
	
	@Test
	public void checkName() {
		Item it=new Item();
		it.setName("doe");
		
		assert("doe".equals(it.getName()));
	
	}

	@Test
	public void chekDisplay() {
		String name = "doe";
		int id = 8;
		float price = 14;
		int nbrElt = 2;
		Item item=new Item();
		item.setName(name);
		item.setId(id);
		item.setPrice(price);
		item.setNbrElt(nbrElt);
		//System.out.println(item.display(name,id,price,nbrElt));
		assert(("doe-8-14.0-2").equals(item.display(name,id,price,nbrElt)));
	}
	
}
